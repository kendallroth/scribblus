import Vue from "vue";

// Components
import App from "./App.vue";

// Utilities
import ComponentsPlugin from "@plugins/ComponentsPlugin";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

// Styles
import "@styles/styles.scss";

Vue.config.productionTip = false;

Vue.use(ComponentsPlugin);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
