const lightColors = [
  "#ffffff",
  "#c1c1c1",
  "#ff0000",
  "#ff7100",
  "#ffe400",
  "#00cc00",
  "#00b2ff",
  "#231fd3",
  "#a300ba",
  "#d37caa",
  "#a0522d",
];

const darkColors = [
  "#000000",
  "#4c4c4c",
  "#740b07",
  "#c23800",
  "#e8a200",
  "#005510",
  "#00569e",
  "#0e0865",
  "#550069",
  "#a75574",
  "#63300d",
];

export const colors = [lightColors, darkColors];
export const weights = [5, 10, 15, 20];
