/** Player */
export interface GamePlayer {
  /** Whether player is active */
  active: boolean;
  /** Player name */
  name: string;
  /** Player rank */
  rank: number;
  /** Player points */
  points: number;
}

/** Chat message */
export interface ChatMessage {
  /** Type of message */
  action: ChatMessageAction;
  /** Message sender */
  player?: string;
  /** Message text */
  text: string;
}

/** Chat message type */
export enum ChatMessageAction {
  GUESS_CORRECT,
  GUESS_INCORRECT,
  PLAYER_JOINED,
  PLAYER_LEFT,
  ROUND_ENDED,
  ROUND_STARTED,
}
