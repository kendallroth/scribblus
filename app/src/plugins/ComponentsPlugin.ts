import { VueConstructor } from "vue/types/vue";

// Components
import SvgIcon from "@components/SvgIcon.vue";

const ComponentsPlugin = {
  install: (Vue: VueConstructor): void => {
    // NOTE: Need to manually register components names because Terser
    //         mangles class names by default...

    Vue.component("SvgIcon", SvgIcon);
  },
};

export default ComponentsPlugin;
