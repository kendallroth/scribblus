const path = require("path");

const src = path.resolve(__dirname, "src");

module.exports = {
  lintOnSave: false,

  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@styles/_vars.scss";
          @import "@styles/_breakpoints.scss";
          @import "@styles/_mixins.scss";
        `,
      },
    },
  },

  configureWebpack: {
    resolve: {
      alias: {
        "@": src,
        "@assets": path.join(src, "assets"),
        "@components": path.join(src, "components"),
        "@config": path.join(src, "config"),
        "@plugins": path.join(src, "plugins"),
        "@styles": path.join(src, "styles"),
        "@typings": path.join(src, "types"),
        "@utilities": path.join(src, "utilities"),
        "@views": path.join(src, "views"),
      },
    },
  },

  pwa: {
    name: "Scribbleus",
  },

  transpileDependencies: ["vuetify"],
};

/* eslint @typescript-eslint/no-var-requires: off */
