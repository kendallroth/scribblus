# Scribbleus

Group drawing game (similar to Pictionary).

<div align="center">
  <img src="designs/initial_work.png" width="500" />
</div>

## Development

| App | Server | Both |
|-----|--------|------|
| Built with [VueJS](https://vuejs.org/) | Build with [NodeJS](https://nodejs.org/) | Tracked with [GitLab](https://gitlab.com/)
| Components from [Vuetify](https://vuejs.org/) | Framework with [ExpressJS](https://expressjs.com/) | Tested with [Jest](https://jestjs.io/)
| Deployed with [Netlify](https://www.netlify.com/) | Deployed with [Heroku](https://www.heroku.com/) |


## Miscellaneous

- Icon generated with [Expo Icon Builder](https://buildicon.netlify.app/)
- PWA assets generated with [`vue-pwa-asset-generator`](https://www.npmjs.com/package/vue-pwa-asset-generator)